package com.suku.kotlin.mvpbase.view

import com.suku.kotlin.mvpbase.R
import com.suku.kotlin.mvpbase.base.BaseActivity
import com.suku.kotlin.mvpbase.presenter.MainActivityPresenter
import com.suku.kotlin.mvpbase.presenter.ibaseview.IMainActivityView
import com.suku.kotlin.mvpbase.presenter.ipresenter.IMainActivityPresenter


class MainActivity : BaseActivity<IMainActivityPresenter, com.suku.kotlin.mvpbase.databinding.ActivityMainBinding>(),
    IMainActivityView {

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onInitializePresenter(): IMainActivityPresenter {
        return MainActivityPresenter(this)
    }

    override fun onPresenterInitialized() {
        viewDataBinding?.clkBut?.setOnClickListener { showMessage("Hellow Kotlin") }
    }
}