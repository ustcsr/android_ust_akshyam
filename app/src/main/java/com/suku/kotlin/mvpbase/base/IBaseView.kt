package com.suku.kotlin.mvpbase.base

import androidx.fragment.app.FragmentActivity

interface IBaseView {

    fun showMessage(message: String)

    fun getActivity(): FragmentActivity

}