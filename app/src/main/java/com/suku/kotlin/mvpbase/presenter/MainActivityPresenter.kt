package com.suku.kotlin.mvpbase.presenter

import android.os.Bundle
import com.suku.kotlin.mvpbase.base.BasePresenter
import com.suku.kotlin.mvpbase.presenter.ibaseview.IMainActivityView
import com.suku.kotlin.mvpbase.presenter.ipresenter.IMainActivityPresenter

class MainActivityPresenter(iBaseView: IMainActivityView) : BasePresenter<IMainActivityView>(iBaseView),
    IMainActivityPresenter {

    override fun onCreate(bundle: Bundle?) {

    }
}