package com.suku.kotlin.mvpbase.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity

abstract class BaseActivity<T : IBasePresenter, V : ViewDataBinding> : AppCompatActivity(), IBaseView {
    protected var TAG = javaClass.simpleName
    protected var iBasePresenter: T? = null
    protected var viewDataBinding: V? = null
    protected var mView: View? = null

    abstract fun getLayoutId(): Int
    abstract fun onInitializePresenter(): T
    open fun onPresenterInitialized() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        iBasePresenter = onInitializePresenter()
        if (iBasePresenter != null)
            if (intent != null) iBasePresenter?.onCreate(intent.extras)
            else iBasePresenter?.onCreate(null)
        onPresenterInitialized()
    }

    override fun onStart() {
        super.onStart()
        iBasePresenter?.onStart()
    }

    override fun onResume() {
        super.onResume()
        iBasePresenter?.onResume()
    }

    override fun onPause() {
        super.onPause()
        iBasePresenter?.onPause()
    }

    override fun onStop() {
        super.onStop()
        iBasePresenter?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        iBasePresenter?.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        iBasePresenter?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        iBasePresenter?.onNewIntent(intent)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun getActivity(): FragmentActivity {
        return this
    }
}