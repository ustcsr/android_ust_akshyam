package com.suku.kotlin.mvpbase.base

import android.content.Intent
import android.os.Bundle

abstract class BasePresenter<T : IBaseView>(protected var iBaseView: T?) : IBasePresenter {

    var TAG = javaClass.simpleName

    override fun onCreate(bundle: Bundle?) {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onPause() {

    }

    override fun onStop() {

    }

    override fun onRestart() {

    }

    override fun onDestroy() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun onNewIntent(intent: Intent?) {

    }
}