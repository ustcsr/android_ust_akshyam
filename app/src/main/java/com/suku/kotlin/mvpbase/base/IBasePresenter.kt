package com.suku.kotlin.mvpbase.base

import android.content.Intent
import android.os.Bundle

interface IBasePresenter {

    fun onCreate(bundle: Bundle?)
    fun onStart()
    fun onResume()
    fun onPause()
    fun onStop()
    fun onRestart()
    fun onDestroy()
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun onNewIntent(intent: Intent?)

}